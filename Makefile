CXX  = g++
FILE1= src/server.cc
FILE2= src/client.cc
OUT1 = server
OUT2 = client

build: build_server build_client

build_client:
	$(CXX) $(FILE2) -o $(OUT2)

build_server:
	$(CXX) $(FILE1) -o $(OUT1)

clean:
	rm -rf $(OUT1) $(OUT2) 
