#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/time.h>

#include "beep.hh"
#include "colours.hh"

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
      beep;
      std::cerr << red << "Usage: ip_address port" << std::endl; exit(0); 
    }
    char *serverIp = argv[1]; int port = atoi(argv[2]); 
   
    char msg[1500]; 
   
    struct hostent* host = gethostbyname(serverIp); 
    sockaddr_in sendSockAddr;   
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
    sendSockAddr.sin_family = AF_INET; 
    sendSockAddr.sin_addr.s_addr = 
        inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
    sendSockAddr.sin_port = htons(port);

    int clientSd = socket(AF_INET, SOCK_STREAM, 0);
    int status = connect(clientSd,
                         (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
    if(status < 0)
    {
      beep;
      std::cout << red << "Error connecting to socket!" << std::endl;
    }
    std::cout << green << "Connected to the server!" << std::endl;
    int bytesRead, bytesWritten = 0;
    struct timeval start1, end1;
    gettimeofday(&start1, NULL);
    while(1)
    {
      std::cout << blue << ">" << white;
      std::string data;
      getline(std::cin, data);
      memset(&msg, 0, sizeof(msg));
      strcpy(msg, data.c_str());
      if(data == "exit")
        {
	  beep;beep;
	  send(clientSd, (char*)&msg, strlen(msg), 0);
	  break;
        }
      bytesWritten += send(clientSd, (char*)&msg, strlen(msg), 0);
      std::cout << green << "Awaiting server response..." << std::endl;
      memset(&msg, 0, sizeof(msg));//clear the buffer
      bytesRead += recv(clientSd, (char*)&msg, sizeof(msg), 0);
      if(!strcmp(msg, "exit"))
        {
	  beep;
	  std::cout << purple << "Server has quit the session" << std::endl;
	  break;
        }
      std::cout << "Server: " << cyan << msg << std::endl;
    }
    gettimeofday(&end1, NULL);
    close(clientSd);
    std::cout << green << "********Session********" << std::endl;
    std::cout << "Bytes written: " << bytesWritten
	      << " Bytes read: " << bytesRead << std::endl;
    std::cout << "Elapsed time: " << (end1.tv_sec- start1.tv_sec) 
	      << " secs" << std::endl;
    std::cout << "Connection closed" << std::endl;
    beep; beep; beep;
    return 0;    
}
