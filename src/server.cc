#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/time.h>

#include "beep.hh"
#include "colours.hh"

int main(int argc, char *argv[]) {
  if(argc != 2) {
    beep;
    std::cerr << red << "Usage: port" << std::endl;
    exit(0);
  }
    
  int port = atoi(argv[1]);
  char msg[1500];
     
  sockaddr_in servAddr;
  bzero((char*)&servAddr, sizeof(servAddr));
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(port);
 
  int serverSd = socket(AF_INET, SOCK_STREAM, 0);
  if(serverSd < 0) {
    beep;std::cerr << red << "Error establishing the server socket" << std::endl;
    exit(0);
  }
  int bindStatus = bind(serverSd, (struct sockaddr*) &servAddr, sizeof(servAddr));
  if(bindStatus < 0) {
    beep;std::cerr << red << "Error binding socket to local address" << std::endl;
    exit(0);
  }
  std::cout << purple << "Waiting for a client to connect..." << std::endl;
  listen(serverSd, 5);

  sockaddr_in newSockAddr;
  socklen_t newSockAddrSize = sizeof(newSockAddr);
  int newSd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
  if(newSd < 0) {
    beep;beep;std::cerr << red << "Error accepting request from client!" << std::endl;
    exit(1);
  }
  beep;beep;std::cout << green << "Connected with client!" << std::endl;
  
  struct timeval start1, end1;
  gettimeofday(&start1, NULL);
  int bytesRead, bytesWritten = 0;
  while(1) {
    std::cout << green << "Awaiting client response..." << std::endl;
    memset(&msg, 0, sizeof(msg));
    bytesRead += recv(newSd, (char*)&msg, sizeof(msg), 0);
    if(!strcmp(msg, "exit")) {
      beep;
      std::cout << purple << "Client has quit the session" << std::endl;
      break;
    }
    std::cout << "Client: " << cyan << msg << std::endl;
    std::cout << blue << ">" << white;
    std::string data;
    getline(std::cin, data);
    memset(&msg, 0, sizeof(msg));
    strcpy(msg, data.c_str());
    if(data == "exit") {
    	beep;beep;send(newSd, (char*)&msg, strlen(msg), 0);beep;beep;
      break;
    }
    bytesWritten += send(newSd, (char*)&msg, strlen(msg), 0);
  }

	gettimeofday(&end1, NULL);
  close(newSd);
  close(serverSd);
  std::cout << green << "********Session********" << std::endl;
  std::cout << "Bytes written: " << bytesWritten
	    << " Bytes read: " << bytesRead << std::endl;
  std::cout << "Elapsed time: " << (end1.tv_sec - start1.tv_sec) 
	    << " secs" << std::endl;
  std::cout << "Connection closed..." << std::endl;
	beep;beep;beep;
  return 0;   
}
